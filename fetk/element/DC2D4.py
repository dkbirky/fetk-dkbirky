import numpy as np

import fetk.dload
import fetk.io.logging as logging
from fetk.util.geometry import edge_normal, cost

from .LND2 import L1D2
from .base import element, PLANE, OFF, ON


class DC2D4(element):
    """4-node fully integrated isoparametric plane diffusive heat transfer element

    Notes
    -----
    - Node and element face numbering

                   [2]
                3-------2
                |       |
           [3]  |       | [1]
                |       |
                0-------1
                   [0]

    - Element is integrated by a 2x2 Gauss rule

    """
    signature = (PLANE, 2, OFF, OFF, OFF, OFF, OFF, OFF, ON, OFF, None, 4)
    properties = {"thickness": {"aliases": ["t", "h"], "default": 1.0}}

    def init(self):
        self.edge = L1D2(A=1)

    def area(self, xc):
        """Returns the area of the triangle

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3, x4 = xc[:, 0]
        y1, y2, y3, y4 = xc[:, 1]
        a = (x1 * y2 - x2 * y1) + (x2 * y3 - x3 * y2)
        a += (x3 * y4 - x4 * y3) + (x4 * y1 - x1 * y4)
        return a / 2.0

    def volume(self, xc):
        """Volume of the quadrilateral, `v = a(xc) * thickness"""
        return self.thickness * self.area(xc)

    def jacobian(self, xc, xg):
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        dNds = self.shapeder(xg)
        dxds = np.dot(dNds, xc)
        return np.linalg.det(dxds)

    def shape(self, xg):
        """Shape functions in the natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        N = np.array(
            [
                (1.0 - s) * (1.0 - t),
                (1.0 + s) * (1.0 - t),
                (1.0 + s) * (1.0 + t),
                (1.0 - s) * (1.0 + t),
            ]
        )
        return N / 4.0

    def shapeder(self, xg):
        """Derivatives of shape functions, wrt to natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t = xg
        dN = np.array(
            [
                [-1.0 + t, 1.0 - t, 1.0 + t, -1.0 - t],
                [-1.0 + s, -1.0 - s, 1.0 + s, 1.0 - s],
            ]
        )
        return dN / 4.0

    def shapegrad(self, xc, xg):
        """Compute the derivatives of shape functions wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        dN : ndarray
            Derivatives of the shape function with respect to the physical
            coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        dNds = self.shapeder(xg)
        dxds = np.dot(dNds, xc)
        dsdx = np.linalg.inv(dxds)
        dNdx = np.dot(dsdx, dNds)
        return dNdx

    @property
    def gauss_points(self):
        """Gauss (integration) points

        Returns
        -------
        gp : ndarray
            s, t = gp[i] are s and t coordinates of the ith Gauss point, assuming
            counter clockwise orientation starting from the lower left corner

        """
        p = 1 / np.sqrt(3.0)
        return np.array([[-p, -p], [p, -p], [p, p], [-p, p]])

    @property
    def gauss_weights(self):
        """Gauss (integration) weights

        Returns
        -------
        gw : ndarray
            w = gw[i] is the ith Gauss weight, assuming counter clockwise
            orientation starting from the lower left corner

        """
        return np.ones(4)

    @property
    def edges(self):
        return [[0, 1], [1, 2], [2, 3], [3, 0]]

    def bmatrix(self, xc, xg):
        """Compute the element B matrix which contains derivatives of shape
        functions, wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        """
        return self.shapegrad(xc, xg)

    def pmatrix(self, xg):
        """Compute the element P matrix

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        P : ndarray
            The S matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        return self.shape(xg)

    def stiffness(self, xc, material, dload):
        ke = self.stiffness1(xc, material)
        if dload is None:
            return ke
        for (type, edge_no, values) in dload.items():
            if type == fetk.dload.surface_film:
                Too, h = values[:2]
                ke += self.stiffness2(xc, edge_no, Too, h)
        logging.debug(f"{self.__class__.__name__}: Element stiffness: {ke}")
        return ke

    def stiffness1(self, xc, material):
        ke = np.zeros((4, 4))
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Be = self.bmatrix(xc, xg)
            Je = self.jacobian(xc, xg)
            Ce = material.conductivity(xc)
            ke += np.dot(np.dot(Be.T, Ce), Be) * Je * wg
        logging.debug(f"{self.__class__.__name__}: Element conduction stiffness: {ke}")
        return ke

    def stiffness2(self, xc, edge_no, Too, h):
        ke = np.zeros((3, 3))
        edge = self.edges[edge_no]
        xd = xc[edge]
        dx, dy = xd[1, 0] - xd[0, 0], xd[1, 1] - xd[0, 1]
        hd = np.sqrt(dx ** 2 + dy ** 2)
        for p in range(len(self.edge.gauss_points)):
            xg = self.edge.gauss_points[p]
            wg = self.edge.gauss_weights[p]
            N = self.edge.shape(xg)
            ix = np.ix_(edge, edge)
            ke[ix] += h * hd / 2 * wg * np.outer(N, N)
        logging.debug(f"{self.__class__.__name__}: Element convection stiffness: {ke}")
        return ke

    def force(self, xc, dload):
        """Calculate the nodal force contribution from distributed and surface loads

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        if dload is None:
            return fe
        for (tag, edge_no, f) in dload.items():
            if tag == fetk.dload.body_flux:
                fe += self.source_array(xc, f[0])
            elif tag == fetk.dload.surface_flux:
                fe += self.conduction_flux_array(xc, edge_no, f[:2])
            elif tag == fetk.dload.surface_film:
                fe += self.convection_flux_array(xc, edge_no, *f[:2])
        return fe

    def source_array(self, xc, amplitude):
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Je = self.jacobian(xc, xg)
            Pe = self.pmatrix(xg)
            if callable(amplitude):
                N = self.shape(xg)
                x = np.dot(N, xc)
                mag = amplitude(x)
            else:
                mag = amplitude
            fe += Je * wg * np.dot(Pe, mag)
        logging.debug(f"{self.__class__.__name__}: Element source array {fe}")
        return fe

    def convection_flux_array(self, xc, edge_no, Too, h):
        fe = self.boundary_flux_array_n(xc, edge_no, Too * h)
        logging.debug(f"{self.__class__.__name__}: Element convection flux array {fe}")
        return fe

    def conduction_flux_array(self, xc, edge_no, q):
        edge = self.edges[edge_no]
        xd = xc[edge]
        n = edge_normal(xd)
        fac = 1 if cost(q[:2], n) > 0 else -1
        qn = fac * np.dot(q[:2], n)
        fe = self.boundary_flux_array_n(xc, edge_no, qn)
        logging.debug(f"{self.__class__.__name__}: Element conduction flux array {fe}")
        return fe

    def boundary_flux_array_n(self, xc, edge_no, qn):
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        edge = self.edges[edge_no]
        xd = xc[edge]
        for p in range(len(self.edge.gauss_points)):
            wg = self.edge.gauss_weights[p]
            xg = self.edge.gauss_points[p]
            N = self.edge.shape(xg)
            dNds = self.edge.shapeder(xg)
            dxds = np.dot(dNds, xd)
            Jd = np.sqrt(dxds[0] ** 2 + dxds[1] ** 2)
            fe[edge] += wg * Jd * N * qn
        return fe
