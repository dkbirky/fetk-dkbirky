import numpy as np

import fetk.dload
from .base import element, LINE, OFF, ON


class LND2(element):
    """An N dimensional 2 node link element

    Parameters
    ----------
    id : int
        The external element ID
    properties : dict
        Element properties
        properties["area"] is the cross sectional area
    material : material
        Element material

    """

    properties = {"area": {"aliases": ["a"], "default": 1.0}}

    def stiffness(self, xc, material, *args):
        """Computes the element stiffness for a nD space truss member

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates [[x1, y1, z1], [x2, y2, z2]]

        Returns
        -------
        K : ndarray
            Element stiffness stored as a (ndof*2, ndof*2) symmetric matrix, where
            ndof is the number of degrees of freedom per node.

        """
        num_nodes = self.num_nodes
        if len(xc.shape) == 1:
            xc = xc.reshape((-1, 1))
        num_dof_per_node = self.num_dof_per_node
        v = xc[1] - xc[0]
        he = np.sqrt(np.dot(v, v))
        ke = np.zeros((num_nodes * num_dof_per_node, num_nodes * num_dof_per_node))
        if self.dimension == 1:
            ke[0, 0] = ke[1, 1] = 1.0
            ke[0, 1] = ke[1, 0] = -1.0
        else:
            n = v[: self.dimension] / he
            nn = np.outer(n, n)
            i, j = num_dof_per_node, num_nodes * num_dof_per_node
            ke[0:i, 0:i] = ke[i:j, i:j] = nn
            ke[0:i, i:j] = ke[i:j, 0:i] = -nn
        return self.area * material.stiffness(xc, ndir=1, nshr=0) / he * ke

    def force(self, xc, dload):
        """Computes the element force for a nD space truss member

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates [[x1, y1, z1], [x2, y2, z2]]
        dltags : ndarray
            Distributed load tag. dltags = 1 if the element has a distributed
            load applied.
        dlvals : ndarray
            Distributed load values.  dlvals[j] is the magnitude of the source
            cooresponding to the condition indicated by dltags.

        Returns
        -------
        F : ndarray
            Element force stored as a (ndof*2,) array, where ndof is the number of
            degrees of freedom per node.

        """
        num_nodes = self.num_nodes
        if len(xc.shape) == 1:
            xc = xc.reshape((-1, 1))
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        if dload is None:
            return fe
        v = xc[1] - xc[0]
        he = np.sqrt(np.dot(v, v))
        for (type, edge_no, f) in dload.items():
            if type != fetk.dload.body_force:
                continue
            for node in range(num_nodes):
                dofs = [num_dof_per_node * node + k for k in range(num_dof_per_node)]
                fe[dofs] = np.array(f[:num_dof_per_node]) * he / 2.0
        return fe


class L1D2(LND2):
    """1D 2 node link element"""

    signature = (LINE, 1, ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, 2)

    @property
    def gauss_weights(self):
        return np.ones(2)

    @property
    def gauss_points(self):
        p = 1 / np.sqrt(3.0)
        return np.array([-p, p])

    def shape(self, xg):
        return np.array([1.0 - xg, 1.0 + xg]) / 2.0

    def shapeder(self, xg):
        return np.array([-1.0, 1.0]) / 2.0


class L2D2(LND2):
    """2D 2 node link element"""

    signature = (LINE, 2, ON, ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, 2)

    @property
    def gauss_weights(self):
        return np.ones(2)

    @property
    def gauss_points(self):
        p = 1 / np.sqrt(3.0)
        return np.array([-p, p])

    def shape(self, xg):
        return np.array(
            [-0.5 * xg * (1.0 - xg), 0.5 * xg * (1.0 + xg), (1.0 - xg) * (1.0 + xg)]
        )

    def shapeder(self, xg):
        return np.array([-0.5 + xg, 0.5 + xg, -2 * xg])


class L3D2(LND2):
    """3D 2 node link element"""

    signature = (LINE, 3, ON, ON, ON, OFF, OFF, OFF, OFF, OFF, OFF, 2)
