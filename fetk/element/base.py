import logging
from fetk.util.lang import classproperty


OFF, ON = 0, 1
LINE = 1
PLANE = 10
HEAT = 20
SOLID = 30


class element:
    """Base element"""

    # The element signature describes the element family and active
    # freedoms.  It is a tuple of length 12 with the following meaning:
    #    signature[0] is the element famly
    #    signature[1] is the element dimension
    #    signature[2] is ON if DOF0 (x displacement) is active
    #    signature[3] is ON if DOF1 (y displacement) is active
    #    signature[4] is ON if DOF2 (z displacement) is active
    #    signature[5] is ON if DOF3 (rotation about the x axis) is active
    #    signature[6] is ON if DOF4 (rotation about the y axis) is active
    #    signature[7] is ON if DOF5 (rotation about the z axis) is active
    #    signature[8] is ON if DOF6 (temperature) is active
    #    signature[9] is ON if DOF7 is active (this is a placeholder)
    #    signature[10] distinguishes elements that otherwise have the same signature
    #    signature[11] is the number of nodes
    signature = (None, None, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, None, None)
    properties = None

    def __init__(self, **properties):
        self._set_properties(properties)
        self.init()

    def __str__(self):
        return type(self).__name__

    def init(self):
        pass

    def _set_properties(self, properties):
        """Set element properties

        Parameters
        ----------
        properties : dict
            value = properties[key]

        Notes
        -----
        Each element

        """
        if self.properties is None:
            if properties:
                logging.warn(f"{self}: fabrication properties not required")
            return
        keys = [_.lower() for _ in list(properties.keys())]
        values = list(properties.values())
        for (name, details) in self.properties.items():
            default = details.get("default")
            name1 = name.lower()
            name2 = name1.replace("_", " ")
            if name1 in keys:
                i = keys.index(name1)
                setattr(self, name, values[i])
                keys[i] = None
                continue
            elif name2 in keys:
                i = keys.index(name2)
                setattr(self, name, values[i])
                keys[i] = None
                continue
            for alias in details["aliases"]:
                if alias in keys:
                    i = keys.index(alias.lower())
                    setattr(self, name, values[i])
                    keys[i] = None
                    break
            else:
                if default is None:
                    raise ValueError(f"{self}: {name!r} must be defined")
                logging.warn(f"{self}: using default {name}={default}")
                setattr(self, name, default)

    def stiffness(self, *args, **kwargs):
        raise NotImplementedError

    def force(self, *args, **kwargs):
        raise NotImplementedError

    def shape(self, *args, **kwargs):
        raise NotImplementedError

    def shapegrad(self, *args, **kwargs):
        raise NotImplementedError

    @classproperty
    def family(self):
        return self.signature[0]

    @classproperty
    def dimension(self):
        return self.signature[1]

    @classproperty
    def nft(self):
        # The node freedom table
        return self.signature[2:-2]

    @classproperty
    def num_dof_per_node(self):
        return sum(self.nft)

    @classproperty
    def active_dofs(self):
        return [i for (i, dof) in enumerate(self.nft) if dof]

    @classproperty
    def special(self):
        return self.signature[-2]

    @classproperty
    def num_nodes(self):
        return self.signature[-1]

    def validate(self, *args):
        return True
