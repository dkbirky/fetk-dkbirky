from types import SimpleNamespace
import numpy as np


def factory(name, elements, connect, element, material):
    return block(name, elements, connect, element, material)


class block:
    """Element block

    Parameters
    ----------
    name : str
        The element block name
    elements : array_like
        The *external global* IDs of elements in this block
    connect : ndarray
        The nodal connectivity
    element : fetk.element
        The element type

    """

    def __init__(self, name, elements, connect, element, material):
        self.name = name
        self.connect = np.array(connect, dtype=int)
        self.element = element
        self.material = material
        self.elements = np.array(elements, dtype=int)
        self.nodes = np.sort(np.unique(connect))
        self.node_map = dict([(n, i) for (i, n) in enumerate(self.nodes)])
        self.elem_map = dict([(e, i) for (i, e) in enumerate(self.elements)])

    @property
    def num_elem(self):
        return self.elements.shape[0]

    def local_dof_map(self, e):
        """The block local element freedom mapping for element `e`.

        Parameters
        ----------
        e : int
            The (internal) element number

        Returns
        -------
        map : list
            map[i] is the block dof cooresponding to the ith element dof

        """
        nodes = self.connect[e]
        ndof = self.element.num_dof_per_node
        return [ndof * self.node_map[n] + k for n in nodes for k in range(ndof)]

    def global_dof_map(self):
        """The global element freedom mapping for element `e`

        Returns
        -------
        map : list
            map[i] is the global dof cooresponding to the ith block dof

        """
        ndof = self.element.num_dof_per_node
        return [ndof * n + k for n in self.nodes for k in range(ndof)]

    def ix1d(self, e):
        """Construct sequence of rows for element `e`.

        Parameters
        ----------
        e : int
            The (internal) element number

        Returns
        -------
        ix : tuple
            The rows in this element block corresponding to element e

        Examples
        --------
        Given the block force fb, the force on element 1 is
        >>> ix = self.ix1d(1)
        >>> fe = fb[ix]

        """
        eft = self.local_dof_map(e)
        return np.ix_(eft)

    def ix2d(self, e):
        """Construct sequence of rows and columns for element `e`.

        Parameters
        ----------
        e : int
            The (internal) element number

        Returns
        -------
        ix : tuple
            The rows and columns in this element block corresponding to element e

        Examples
        --------
        Given the block stiffness kb, the stiffness for element 1 is
        >>> ix = self.ix2d(1)
        >>> ke = kb[ix]

        """
        eft = self.local_dof_map(e)
        return np.ix_(eft, eft)

    def force(self, coords, dloads):
        """Assemble force over block

        Parameters
        ----------
        coords : ndarray
            Nodal coordinates.  coords[n, i] is the ith coordinate of node n

        Returns
        -------
        fb : ndarray
            Block force arrays tored as a full (nd x n) array, where nd
            is the number of degrees of freedom per node and n the total number
            of nodes.

        """
        num_dof_per_node = self.element.num_dof_per_node
        num_nodes = self.nodes.shape[0]
        num_dofs = num_nodes * num_dof_per_node

        fb = np.zeros(num_dofs)
        for (iel, xel) in enumerate(self.elements):
            nodes = self.connect[iel]
            xc = coords[nodes]
            dload = dloads.get(xel)
            fe = self.element.force(xc, dload)
            fb[self.ix1d(iel)] += fe
        return fb

    def stiffness(self, coords, dloads=None):
        """Assemble the block finite element stiffness

        Parameters
        ----------
        coords : ndarray
            Nodal coordinates.  coords[n, i] is the ith coordinate of node n

        Returns
        -------
        kb : ndarray
            Block stiffness matrix stored as a full (nd*n, nd*n) symmetric matrix,
            where nd is the number of degrees of freedom per node and n the total
            number of nodes.

        """
        num_dof_per_node = self.element.num_dof_per_node
        num_nodes = self.nodes.shape[0]

        kb = np.zeros((num_nodes * num_dof_per_node, num_nodes * num_dof_per_node))
        for (iel, nodes) in enumerate(self.connect):
            xc = coords[nodes]
            dload = None if dloads is None else dloads.get(self.elements[iel])
            ke = self.element.stiffness(xc, self.material, dload)
            kb[self.ix2d(iel)] += ke

        return kb
