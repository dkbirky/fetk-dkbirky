import numpy as np

from .LND2 import L1D2
from .base import element, PLANE, OFF, ON

import fetk.dload
import fetk.io.logging as logging
from fetk.util.geometry import edge_normal, cost


class DC2D3(element):
    signature = (PLANE, 2, OFF, OFF, OFF, OFF, OFF, OFF, ON, OFF, None, 3)
    properties = {"thickness": {"aliases": ["t", "h"], "default": 1.0}}

    def init(self):
        self.edge = L1D2(A=1)

    def area(self, xc):
        """Returns the area of the triangle

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        a : float
            The area

        """
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        a = (x1 * y2 - x2 * y1) - (x1 * y3 - x3 * y1) + (x2 * y3 - x3 * y2)
        return a / 2.0

    def volume(self, xc):
        """Volume of the quadrilateral, `v = a(xc) * thickness"""
        return self.thickness * self.area(xc)

    def jacobian(self, xc):
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        return 2 * self.area(xc)

    def shape(self, xc, p):
        """Shape functions in the triangle (natural) coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates
        p : ndarray
            Gauss point coordinate

        Returns
        -------
        N : ndarray
            N[i] is the ith shape function evaluated at p

        """
        x, y = self.isop_map(xc, p)
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        N1 = x2 * y3 - x3 * y2 + (y2 - y3) * x + (x3 - x2) * y
        N2 = x3 * y1 - x1 * y3 + (y3 - y1) * x + (x1 - x3) * y
        N3 = x1 * y2 - x2 * y1 + (y1 - y2) * x + (x2 - x1) * y
        Ae = self.area(xc)
        return np.array([N1, N2, N3]) / Ae / 2.0

    def shapegrad(self, xc):
        """Derivatives of shape functions, wrt to triangle (natural) coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates

        Returns
        -------
        dN : ndarray
            dN[i] is the derivative of ith shape function

        """
        Ae = self.area(xc)
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        dN = np.array([[y2 - y3, y3 - y1, y1 - y2], [x3 - x2, x1 - x3, x2 - x1]])
        return dN / Ae / 2.0

    @property
    def gauss_points(self):
        return np.array([[1.0, 1.0], [4.0, 1.0], [1.0, 4.0]]) / 6.0

    @property
    def gauss_weights(self):
        return np.ones(3) / 3.0

    @property
    def edges(self):
        return [[0, 1], [1, 2], [2, 0]]

    def isop_map(self, xc, p):
        """Map the point `p` in natural coordinates to physical coordinates

        Paratemeters
        ------------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=3) are the x coordinates, in the order shown above
            xc[:, 1] (len=3) are the y coordinates
        p : list of float
            s, t = p

        """
        s, t = p
        x1, x2, x3 = xc[:3, 0]
        y1, y2, y3 = xc[:3, 1]
        x = x1 * s + x2 * t + x3 * (1 - s - t)
        y = y1 * s + y2 * t + y3 * (1 - s - t)
        return x, y

    def bmatrix(self, xc):
        return self.shapegrad(xc)

    def pmatrix(self, xc, p):
        return self.shape(xc, p)

    def stiffness(self, xc, material, dload):
        ke = self.stiffness1(xc, material)
        if dload is None:
            return ke
        for (type, edge_no, values) in dload.items():
            if type == fetk.dload.surface_film:
                Too, h = values[:2]
                ke += self.stiffness2(xc, edge_no, Too, h)
        logging.debug(f"{self.__class__.__name__}: Element stiffness: {ke}")
        return ke

    def stiffness1(self, xc, material):
        ke = np.zeros((3, 3))
        Be = self.bmatrix(xc)
        Ae = self.area(xc)
        for p in range(3):
            wg = self.gauss_weights[p]
            ke += Ae * np.dot(np.dot(Be.T, material.conductivity(xc)), Be) * wg
        logging.debug(f"{self.__class__.__name__}: Element conduction stiffness: {ke}")
        return ke

    def stiffness2(self, xc, edge_no, Too, h):
        ke = np.zeros((3, 3))
        edge = self.edges[edge_no]
        xd = xc[edge]
        dx, dy = xd[1, 0] - xd[0, 0], xd[1, 1] - xd[0, 1]
        hd = np.sqrt(dx ** 2 + dy ** 2)
        for p in range(len(self.edge.gauss_points)):
            xg = self.edge.gauss_points[p]
            wg = self.edge.gauss_weights[p]
            N = self.edge.shape(xg)
            ix = np.ix_(edge, edge)
            ke[ix] += h * hd / 2 * wg * np.outer(N, N)
        logging.debug(f"{self.__class__.__name__}: Element convection stiffness: {ke}")
        return ke

    def force(self, xc, dload):
        """Calculate the nodal force contribution from distributed and surface loads

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        if dload is None:
            return fe
        for (tag, edge_no, f) in dload.items():
            if tag == fetk.dload.body_flux:
                fe += self.source_array(xc, f[0])
            elif tag == fetk.dload.surface_flux:
                fe += self.conduction_flux_array(xc, edge_no, f[:2])
            elif tag == fetk.dload.surface_film:
                fe += self.convection_flux_array(xc, edge_no, *f[:2])
        return fe

    def source_array(self, xc, amplitude):
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        Ae = self.area(xc)
        he = self.thickness
        for p in range(3):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            N = self.shape(xc, xg)
            if callable(amplitude):
                x = np.dot(N, xc)
                mag = amplitude(x)
            else:
                mag = amplitude
            fe += Ae * he * wg * mag * N
        logging.debug(f"{self.__class__.__name__}: Element source array: {fe}")
        return fe

    def convection_flux_array(self, xc, edge_no, Too, h):
        fe = self.boundary_flux_array_n(xc, edge_no, Too * h)
        logging.debug(f"{self.__class__.__name__}: Element convection flux array: {fe}")
        return fe

    def conduction_flux_array(self, xc, edge_no, q):
        edge = self.edges[edge_no]
        xd = xc[edge]
        n = edge_normal(xd)
        fac = 1 if cost(q[:2], n) > 0 else -1
        qn = fac * np.dot(q[:2], n)
        fe = self.boundary_flux_array_n(xc, edge_no, qn)
        logging.debug(f"{self.__class__.__name__}: Element conduction flux array: {fe}")
        return fe

    def boundary_flux_array_n(self, xc, edge_no, qn):
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        edge = self.edges[edge_no]
        xd = xc[edge]
        dx, dy = xd[1, 0] - xd[0, 0], xd[1, 1] - xd[0, 1]
        hd = np.sqrt(dx ** 2 + dy ** 2)
        for p in range(2):
            xg = self.edge.gauss_points[p]
            wg = self.edge.gauss_weights[p]
            N = self.edge.shape(xg)
            fe[edge] += hd / 2 * wg * N * qn
        return fe
