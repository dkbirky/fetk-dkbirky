import os
import shutil
from contextlib import contextmanager


@contextmanager
def working_dir(dirname):
    old_cwd = os.getcwd()
    os.chdir(dirname)
    yield
    os.chdir(old_cwd)


def force_remove(file):
    if os.path.isfile(file):
        os.remove(file)
    elif os.path.isdir(file):
        shutil.rmtree(file)
