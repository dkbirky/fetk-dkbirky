================
Input file usage
================

Input file keyword: ``materials``

This option is used to define a *list* of materials directly by specifying their
material ID, model name, and model parameters.

------------------
Defining materials
------------------

Each material in the list of materials defines:

* The material ID.  This ID is used to reference the material in element blocks.
* The model name.  Eg, "elastic".
* The model parameters.

Repeat this entry for every material in the model.

-------
Example
-------

.. code-block:: yaml

    materials:
    - material:
        id: 1
        model: elastic
        parameters:
          modulus: 10.
    - material:
        id: 2
        model: elastic
        parameters:
          modulus: 20.
