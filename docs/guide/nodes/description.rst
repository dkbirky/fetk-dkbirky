===================
General Description
===================

Nodes are defined by giving their coordinates with respect to a rectangular Cartesian coordinate system :math:`(x, y, z)` called the global system. See :ref:`fig_node_location`

.. _fig_node_location:

.. figure:: ./location.png
   :width: 4in

   Location of node :math:`n` in 3D space :math:`(x, y, z)`

Attached to each node :math:`n` there is a local Cartesian coordinate system
:math:`(\overline{x}, \overline{y}, \overline{z})`, which is used to specify
freedom directions and is called the Freedom Coordinate System or ``fcs``. The
definition of the ``fcs`` is not part of the data structures introduced in this
Chapter, because that information pertains to the freedom data structures. It is
treated in :ref:`freedom_coordinate_system`.

Nodes are classified into primary and auxiliary. Primary nodes or P-nodes do
double duty: specification of element geometry as well as residence for degrees
of freedom. Auxiliary nodes or A-nodes are used only for geometric purposes and
bear no degrees of freedom.

----------------------------
Position and Direction Nodes
----------------------------

Another classification of nodes distinguishes between position and orientation
or direction nodes. The former are points with finite coordinates. The latter
are directions or "points at infinity" which are often used to define local
coordinate systems. In the data structures described here position and
orientation nodes are placed in the same table and are treated by the same
methods. This unification is possible thanks to the use of homogeneous nodal
coordinates: four numbers may be given instead of three.

Although orientation nodes are usually of auxiliary type, sometimes they carry
degrees of freedom and thus are categorized as primary. This situation occurs in
the definition of infinite elements (in topic treated in Advanced Finite Element
Methods), which have node points at infinity.

-----------------------
External identification
-----------------------

Nodes are defined by the user. They are identified by unique positive numbers in
the range 1 through ``laxnod`` where ``laxnod``, which stands for largest
external node, is a problem parameter. These identifiers are called external
node numbers. All communication with the user employs external numbers. When it
is important to distinguish external node numbers from the internal node numbers
defined below, the former will be shown as **boldface** numbers, as in the
examples :eq:`ex_node_1` and :eq:`ex_node_2`.

External node numbers need not be consecutive. That is, "gaps" may appear. For
example, the user may define 6 nodes numbered

.. math::
   :label: ex_node_1

   \mathbf{2}, \
   \mathbf{4}, \
   \mathbf{5}, \
   \mathbf{8}, \
   \mathbf{14}, \
   \mathbf{18}

Then ``laxnod`` is **18** but the number of defined nodes, called ``numnod``, is
6.

-----------------------
Internal identification
-----------------------

External nodes are stored consecutively in tables for use by the program. The
index for table retrieval is called internal node number. For the previous
example:

.. math::
   :label: ex_node_2

   \begin{tabular}{lcccccc}
   \text{External:} & $\mathbf{2}$ & $\mathbf{4}$ & $\mathbf{5}$ & $\mathbf{8}$ & $\mathbf{14}$ & $\mathbf{18}$ \\
   \text{Internal:} & 0          & 1          & 2          & 3          & 4           & 5
   \end{tabular}
