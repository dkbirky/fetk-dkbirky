================
Input file usage
================

Input file keyword: ``node``

This option is used to define a *list* of nodes directly by specifying their coordinates.

--------------
Defining nodes
--------------

Each item in the list of nodes contains:

1. The node number.
2. First coordinate of the node.
3. Second coordinate of the node.  Optional, default is zero.
4. Third coordinate of the node.  Optional, default is zero.

Repeat this entry for every node in the model.

-------
Example
-------

.. code-block:: yaml

    node:
    - 1, 0., 0.
    - 2, 1., 0.
    - 3, 2., 0.
