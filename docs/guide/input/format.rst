========================
Format of the input file
========================

The example of a truss, shown in Figure :ref:`fig_truss`, is used to illustrate the basic format of the ``fem`` input file. The truss is a simple, pin-jointed framework that is and mounted on rollers on its bottom. The members can rotate freely at the joints. The frame is prevented from moving out of plane. A simulation is performed to determine the structure's deflection when three 10 kN loads are applied as shown in Figure :ref:`fig_truss`.

.. _fig_truss:

.. figure:: ./truss.jpg
   :width: 4in

   Schematic of a truss.

The ``fem`` input file is compact and easily understood. The complete input file
for this example, which is shown in Listing :ref:`listing_truss_inp` is split
into two distinct parts. The first section contains model data and includes all
the information required to define the structure being analyzed. The second
section contains data that define what happens to the model: the sequence of
loading or events for which the response of the structure is required.

.. code-block:: yaml
   :name: listing_truss_inp
   :caption: Basic truss input file

   node:
   - 1, 0., 0.
   - 2, 4., 2.
   - 3, 8., 4.
   - 4, 8., 0.
   - 5, 12., 2.
   - 6, 16., 0.
   element:
   - 1, 1, 2
   - 2, 1, 4
   - 3, 2, 4
   - 4, 2, 3
   - 5, 3, 4
   - 6, 3, 5
   - 7, 4, 5
   - 8, 4, 6
   - 9, 5, 6
   element blocks:
   - block:
       id: 1
       material: 1
       elements: 1, 2, 3, 4, 5, 6, 7, 8, 9
       element properties:
         area: 1.
   materials:
   - material:
       id: 1
       model: elastic
       parameters:
         modulus: 200e9
   boundary:
   - 1, y, 0.
   - 6, y, 0.
   cload:
   - 2, y, -20e3
   - 3, y, -20e3
   - 3, y, -20e3


The input file is composed of a number of sections that contain data describing a part of the model.

--------------
Input sections
--------------

Each section is defined by a keyword followed by a colon (:).  For example, ``node:`` is the keyword for specifying the nodal coordinates, and ``element:`` is the keyword for specifying the element connectivity.

----------
Data lines
----------

Input sections are followed by data lines.  Examples of such data include nodal coordinates or element connectivities. For example, the section defining the nodes for the truss model is:

.. code-block:: yaml

   node:
   - 1, 0., 0.
   - 2, 4., 2.
   - 3, 8., 4.
   - 4, 8., 0.
   - 5, 12., 2.
   - 6, 16., 0.

The first piece of data in each data line is an integer that defines the node number. The second and third entries are floating-point numbers that specify the :math:`x` and :math:`y` coordinates of the node.

Data items are separated by commas.
